# An Automatically Generated Netlist Parser

Before Using :
 - This is Run on Ubuntu by Virual Box.
 - The ScalableVectorgraphicsGenerator.cpp is for drawing in browser.

#### console
````sh
flex Compilerlexyacc.lex  
yacc -d Compilerlexyacc.y  
g++ lex.yy.c y.tab.c Compilerlexyacc.cpp ScalableVectorGraphicsGenerator.cpp -lfl -o a.out  
./a.out <design.isc >a.html  
````

![Result](outputimg.png)


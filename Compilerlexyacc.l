%{
#include "y.tab.h" 
int commentcheck = 1;
%}
%%
[0-9]+ 	{	
			if(commentcheck)
			{
				yylval.number = atoi(yytext);
				return NUMBER; 
			}
		}
INPUT { if(commentcheck) return INPUT; }
OUTPUT { if(commentcheck) return OUTPUT; }
BUFF { if(commentcheck) return BUFF; }
NOT { if(commentcheck) return NOT; }
AND { if(commentcheck) return AND; }
NAND { if(commentcheck) return NAND; }
OR { if(commentcheck) return OR; }
NOR { if(commentcheck) return NOR; }
XOR { if(commentcheck) return XOR; }
NXOR { if(commentcheck) return NXOR; }
"=" { if(commentcheck) return ASSIGN; }
"#" { commentcheck = 0; return COMMENT; }
"," { if(commentcheck) return COMMA; }
[ \t] {;} 
[\n] { commentcheck = 1; return NLINE; }
. { if(commentcheck) return yytext[0]; }
%%
int yywrap()
{
	return 1;
}
#include "ScalableVectorGraphicsGenerator.hpp"
#include<iostream>
using namespace std;
ScalableVectorGraphicsGenerator::ScalableVectorGraphicsGenerator()
    : gateSize(50)
    , canvasWidth(1000)
    , canvasHeight(1000)
{
}

ScalableVectorGraphicsGenerator::~ScalableVectorGraphicsGenerator()
{
}

void ScalableVectorGraphicsGenerator::drawRectangle(int x, int y, int width, int height, std::string color)
{
    cout << "\t<rect "
        << "x=\"" << x << "\" "
        << "y=\"" << y << "\" "
        << "width=\"" << width << "\" "
        << "height=\"" << height << "\" ";
    cout << "fill=\""
        << color
        << "\" ";
    cout << " />" << endl;
}

void ScalableVectorGraphicsGenerator::drawAND(int x, int y)
{
   cout << "\t<path d=";
   cout << "\"M "
        << x << " " << y << " "
        << x + gateSize << " " << y << " "
        << "Q "
        << x + gateSize * 1.5 << " " << y + gateSize / 2 << " "
        << x + gateSize << " " << y + gateSize << " "
        << "L "
        << x << " " << y + gateSize << " "
        << "Z "
        << "\" fill=\"red\" />";
   cout << endl;
}

void ScalableVectorGraphicsGenerator::drawOR(int x, int y)
{
   cout << "\t<path d=";
   cout << "\"M "
        << x << " " << y << " "
        << x + gateSize << " " << y << " "
        << "Q "
        << x + gateSize * 1.5 << " " << y + gateSize / 2 << " "
        << x + gateSize << " " << y + gateSize << " "
        << "L "
        << x << " " << y + gateSize << " "
        << "Q "
        << x + gateSize / 2 << " " << y + gateSize / 2 << " "
        << x << " " << y << " "
        << "\" fill=\"yellow\" />";
   cout << endl;
}

void ScalableVectorGraphicsGenerator::drawINV(int x, int y)
{
   cout << "\t<path d=";
   cout << "\"M "
        << x << " " << y << " "
        << "L "
        << x + gateSize + gateSize / 4 << " " << y + gateSize / 2 << " "
        << x << " " << y + gateSize << " "
        << "Z "
        << "\" fill=\"green\" />";
   cout << endl;

   cout << "\t<circle ";
   cout << "cx=" << x + gateSize + gateSize / 4 << " "
        << "cy=" << y + gateSize / 2 << " "
        << "r=" << gateSize / 7 << " "
        << "fill=\"green\" />";
   cout << endl;
}

void ScalableVectorGraphicsGenerator::drawV(int x, int y)
{
	cout << "\t<path d=";
	cout << "\"M "
		<< x << " " << y << " "
		<< "L "
		<< x + gateSize + gateSize / 4 << " " << y + gateSize / 2 << " "
		<< x << " " << y + gateSize << " "
		<< "Z "
		<< "\" fill=\"green\" />";
	cout << endl;
}

void ScalableVectorGraphicsGenerator::drawNAND(int x, int y)
{
    cout << "\t<path d=";
    cout << "\"M "
        << x << " " << y << " "
        << x + gateSize << " " << y << " "
        << "Q "
        << x + gateSize * 1.5 << " " << y + gateSize / 2 << " "
        << x + gateSize << " " << y + gateSize << " "
        << "L "
        << x << " " << y + gateSize << " "
        << "Z "
        << "\" fill=\"red\" />";
    cout << endl;

    cout << "\t<circle ";
    cout << "cx=" << x + gateSize + gateSize / 3 << " "
        << "cy=" << y + gateSize / 2 << " "
        << "r=" << gateSize / 7 << " "
        << "fill=\"red\" />";
    cout << endl;
}

void ScalableVectorGraphicsGenerator::drawNOR(int x, int y)
{
    cout << "\t<path d=";
    cout << "\"M "
        << x << " " << y << " "
        << x + gateSize << " " << y << " "
        << "Q "
        << x + gateSize * 1.5 << " " << y + gateSize / 2 << " "
        << x + gateSize << " " << y + gateSize << " "
        << "L "
        << x << " " << y + gateSize << " "
        << "Q "
        << x + gateSize / 2 << " " << y + gateSize / 2 << " "
        << x << " " << y << " "
        << "\" fill=\"yellow\" />";
    cout << endl;

    cout << "\t<circle ";
    cout << "cx=" << x + gateSize + gateSize / 3 << " "
        << "cy=" << y + gateSize / 2 << " "
        << "r=" << gateSize / 7 << " "
        << "fill=\"yellow\" />";
    cout << endl;
}

void ScalableVectorGraphicsGenerator::drawNXOR(int x, int y)
{
    cout << "\t<path d=";
    cout << "\"M "
        << x + gateSize / 4 << " " << y << " "
        << x + gateSize << " " << y << " "
        << "Q "
        << x + gateSize * 1.5 << " " << y + gateSize / 2 << " "
        << x + gateSize << " " << y + gateSize << " "
        << "L "
        << x + gateSize / 4 << " " << y + gateSize << " "
        << "Q "
        << x + gateSize / 2 + gateSize / 4 << " " << y + gateSize / 2 << " "
        << x + gateSize / 4 << " " << y << " "
        << "\" fill=\"blue\" />";
    cout << endl;

    cout << "\t<path d=";
    cout << "\"M "
        << x << " " << y << " "
        << x + gateSize / 8 << " " << y << " "
        << "Q "
        << x + gateSize / 2 << " " << y + gateSize / 2 << " "
        << x + gateSize / 8 << " " << y + gateSize << " "
        << "L "
        << x << " " << y + gateSize << " "
        << "Q "
        << x + gateSize / 2 << " " << y + gateSize / 2 << " "
        << x << " " << y << " "
        << "\" fill=\"blue\" />";
    cout << endl;

    cout << "\t<circle ";
    cout << "cx=" << x + gateSize + gateSize / 3 << " "
        << "cy=" << y + gateSize / 2 << " "
        << "r=" << gateSize / 7 << " "
        << "fill=\"blue\" />";
    cout << endl;
}

void ScalableVectorGraphicsGenerator::drawXOR(int x, int y)
{
    cout << "\t<path d=";
    cout << "\"M "
        << x + gateSize / 4 << " " << y << " "
        << x + gateSize << " " << y << " "
        << "Q "
        << x + gateSize * 1.5 << " " << y + gateSize / 2 << " "
        << x + gateSize << " " << y + gateSize << " "
        << "L "
        << x + gateSize / 4 << " " << y + gateSize << " "
        << "Q "
        << x + gateSize / 2 + gateSize / 4 << " " << y + gateSize / 2 << " "
        << x + gateSize / 4 << " " << y << " "
        << "\" fill=\"pink\" />";
    cout << endl;

    cout << "\t<path d=";
    cout << "\"M "
        << x << " " << y << " "
        << x + gateSize / 8 << " " << y << " "
        << "Q "
        << x + gateSize / 2 << " " << y + gateSize / 2 << " "
        << x + gateSize / 8 << " " << y + gateSize << " "
        << "L "
        << x << " " << y + gateSize << " "
        << "Q "
        << x + gateSize / 2 << " " << y + gateSize / 2 << " "
        << x << " " << y << " "
        << "\" fill=\"pink\" />";
    cout << endl;
}

void ScalableVectorGraphicsGenerator::drawIOBox(int x, int y, int flag)
{
    if (flag == 0) {
        drawRectangle(x, y + gateSize / 4, gateSize, gateSize / 2, "Cyan");
    }
    else if ( flag == 1 ){
        drawRectangle(x, y + gateSize / 4, gateSize, gateSize / 2, "Fuchsia");
    } else {
        drawRectangle(x, y + gateSize / 4, gateSize, gateSize / 2, "gray");
    }
}

void ScalableVectorGraphicsGenerator::drawPath(std::vector< int >& path)
{
    cout << "\t<path d=";
    cout << "\"M " << path[0] << " " << path[1] << " ";
    cout << "L ";
    for (int i = 2; i < path.size(); ++i)
        cout << path[i] << " ";
    cout << "\" stroke=\"black\" />";
    cout << endl;
}

void ScalableVectorGraphicsGenerator::drawText(int x, int y, int text)
{
    cout << "\t<text "
        << "x=\"" << x + gateSize / 2 << "\" "
        << "y=\"" << y + gateSize / 2 << "\" >"
        << text << "</text>";
    cout << endl;
}

void ScalableVectorGraphicsGenerator::SVGBegin(std::string filename, int width, int height)
{
    out.open(std::string(filename+".html").c_str(), std::ios::out);
    cout << "<svg "
        << "width=\"" << width << "px\" "
        << "height=\"" << height << "px\">" << endl;
    canvasWidth = width;
    canvasHeight = height;
}

void ScalableVectorGraphicsGenerator::SVGEnd()
{
    cout << "</svg>" << endl;
    out.close();
}

void ScalableVectorGraphicsGenerator::setGateSize(int inSize)
{
    gateSize = inSize;
}

int ScalableVectorGraphicsGenerator::getGateSize()
{
    return gateSize;
}
